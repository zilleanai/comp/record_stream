"""
    record_stream
    ~~~~

    Component for record_stream in general.

    :copyright: Copyright © 2019 chriamue
    :license: Not open source, see LICENSE for details
"""

__version__ = '0.1.0'


from flask_unchained import Bundle


class RecordStreamBundle(Bundle):

    name = 'record_stream_bundle'

    @classmethod
    def before_init_app(cls, app):
        pass

    @classmethod
    def after_init_app(cls, app):
        pass
