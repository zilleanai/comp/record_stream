import os
import json
import numpy as np
import cv2
import time
from datetime import datetime
from flask import current_app, make_response, request, jsonify, abort, send_file
from flask_unchained import BundleConfig, Controller, route, injectable
from backend.config import Config as AppConfig
from ..tasks import record_async_task


class RecordStreamController(Controller):

    @route('/record/<string:project>', methods=['POST'])
    def record(self, project):
        BASE_DIR = os.path.join(
            BundleConfig.current_app.config.DATA_FOLDER, project)
        if not os.path.exists(BASE_DIR):
            return abort(404)
        url = request.json['url']
        vcap = cv2.VideoCapture(url)
        frames = []
        for i in range(50):
            ret, frame = vcap.read()
            if ret == True:
                frames.append(frame)
            time.sleep(0.5)
        if len(frames) < 1:
            return jsonify(success=False)

        folder = os.path.join(BASE_DIR, str(datetime.now().strftime('%Y-%m-%d_%H-%M-%S')))
        os.makedirs(folder)
        for i, frame in enumerate(frames):
            filename = os.path.join(folder, str(i)+'.jpg')
            cv2.imwrite(filename, frame)
        vcap.release()
        return jsonify(success=True)
