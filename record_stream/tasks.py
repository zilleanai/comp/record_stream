import os
from flask_unchained.bundles.celery import celery
from backend.config import Config as AppConfig

@celery.task(serializer='json')
def record_async_task(project, url):
    BASE_DIR = os.path.join(
        AppConfig.DATA_FOLDER, project)

