import { get, post } from 'utils/request'
import { v1 } from 'api'
import { storage } from 'comps/project'

function record_stream(uri) {
    return v1(`/record_stream${uri}`)
}

export default class RecordStream {

    static record({ url }) {
        return post(record_stream(`/record/${storage.getProject()}`), { 'url': url })
    }
}
