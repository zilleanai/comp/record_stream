import React from 'react'
import Helmet from 'react-helmet'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { reduxForm, change } from 'redux-form'
import formActions from 'redux-form/es/actions'
import { bindRoutineCreators } from 'actions'
import { injectReducer, injectSagas } from 'utils/async'
import { PageContent } from 'components'
import { TextField } from 'components/Form'
import { Steps } from 'intro.js-react';
import 'intro.js/introjs.css';
import { Player } from 'video-react';
import "video-react/dist/video-react.css";
import { record } from '../../actions'
import { storage } from 'comps/project'
const FORM_NAME = 'record'

class RecordStream extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      stepsEnabled: true,
      initialStep: 0,
      url: 'https://media.w3.org/2010/05/sintel/trailer_hd.mp4',
      steps: [
        {
          element: '.record-stream-url',
          intro: 'Put an url of a video stream here.',
        },
        {
          element: '.button-primary',
          intro: 'Press this button to record the stream and save images to the project.',
        },
        {
          element: '.player',
          intro: 'Preview the stream here.',
        }
      ],
    }
  }

  render() {
    const { isLoaded, error, handleSubmit, pristine, submitting } = this.props
    const { stepsEnabled, steps, initialStep, url } = this.state;
    if (!isLoaded) {
      return null
    }
    return (
      <PageContent>
        <Steps
          enabled={stepsEnabled}
          steps={steps}
          initialStep={initialStep}
          onExit={() => {
            this.setState(() => ({ stepsEnabled: false }));
          }}
        />
        <Helmet>
          <title>Record Stream</title>
        </Helmet>
        <form onSubmit={handleSubmit(record)}>
          <TextField className='record-stream-url' name='url'
            onChange={(e) => this.setState({ url: e.target.value })} />
          <div className="row">
            <button type="submit"
              className="button-primary"
              disabled={pristine || submitting}
            >
              {submitting ? 'Recording...' : 'Record'}
            </button>
          </div>
        </form>
        <Player className='player'
          playsInline
          src={url}
        />

      </PageContent>
    )
  }
}


const withConnect = connect(
  (state, props) => {
    const isLoaded = true;
    return {
      isLoaded
    }
  },
  (dispatch) => bindRoutineCreators({}, dispatch),
)


const withForm = reduxForm({
  form: FORM_NAME
})

const withSaga = injectSagas(require('comps/record_stream/sagas/record'))

export default compose(
  withSaga,
  withForm,
  withConnect,
)(RecordStream)
