import { call, put, takeLatest } from 'redux-saga/effects'

import { record } from '../actions'
import { createRoutineFormSaga } from 'sagas'
import RecordStreamApi from '../api'


export const KEY = 'record'

export const recordSaga = createRoutineFormSaga(
  record,
  function* successGenerator(payload) {
    const response = yield call(RecordStreamApi.record, payload)
    yield put(record.success(response))
  }
)

export default () => [
  takeLatest(record.TRIGGER, recordSaga)
]
